
<!-- PHP code can be included to another file by using the require_once keyword. -->
<!-- We will use this method to separate the declaration of variables and functions from the HTML content. -->
<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control Structures</title>
</head>
<body>

	<!-- <h1>Hello World</h1> -->

	<h1>Echoing Values</h1>

	<p><?php echo 'Good day $name! Your given email is $email.'; ?></p>
	<p><?= "Good day $name! Your given email is $email."; ?></p>
	<p><?php echo 'Good day ' . $name . '! Your given email is ' . $email . '.'; ?></p>
	<p><?php echo PI; ?></p>
	<p><?php echo STUDENT ?></p>
	<p><?php echo $STUDENT ?></p>

	<!-- Boolean -->
	<p><?php echo $hasTravelledAbroad; ?></p>
	<p><?php echo $hasSymptoms; ?></p>

	<!-- Null -->
	<p><?php echo $middleName; ?></p>

	<!-- To see these data types, we can use var_dump() or gettype() function -->
	<p><?php echo gettype($hasTravelledAbroad); ?></p>

	<!-- To see more details about the variable -->
	<p><?php echo var_dump($hasTravelledAbroad); ?></p>


	<!-- Array -->
	<p><?php echo $grades[2];?></p>
	<p><?php echo $grades[3];?></p>
	<p><?= implode(', ', $grades); ?></p>
	<p><?php echo var_dump($grades);?></p>
	<p><?php print_r($grades);?></p>

	<!-- Objects -->
	<p><?php echo $gradesObject->firstGrading; ?></p>
	<p><?php echo $personObj->address->country; ?></p>


	<h1>Operators</h1>
	<p>X: <?php echo $x;?></p>
	<p>Y: <?php echo $y;?></p>

	<p>Is Legal Age: <?php echo var_dump($isLegalAge);?></p>
	<p>Is Registered: <?php echo var_dump($isRegistered);?></p>

	<h2>Arithmetic Operators</h2>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Dif: <?php echo $x - $y; ?></p>
	<p>Prod: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>

	<h2>Equality Operator</h2>
	<p>Loose Equality: <?php echo var_dump($x == '1342.14');?></p>
	<p>Strict Equality: <?php echo var_dump($x === '1342.14');?></p>

	<h3>Greater/Lesser Operator</h3>
	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater: <?php echo var_dump($x > $y); ?></p>

	<h2>Logical Operators</h2>
	<p>Are All Requirements met?: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are Some Requirements met?: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are Some Requirements Not met?: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>

	<h1>Function</h1>

	<p>Full Name: <?php echo getFullName('John', 'Danes', 'Smith'); ?></p>
	<p>Full Name: <?php echo getFullName('John', 'Jackson', 'Smith'); ?></p>

	<h1>Selection Control Structure</h1>

	<h3>If-Elseif-Else Statement</h3>
	<p><?php echo determineTyphoonIntensity(67); ?></p>


	<h3>Ternary Operator</h3>
	<p>78: <?php echo var_dump(isUnderAge(78)); ?></p>
	<p>17: <?php echo var_dump(isUnderAge(17)); ?></p>

	<h3>Switch Statement</h3>
	<p><?php echo determineComputerUser(5); ?></p>


	<h2>Try-Catch-Finally</h2>
	<p><?php echo greeting("Hello World"); ?></p>
	<p><?php echo greeting(12); ?></p>
	



</body>
</html>