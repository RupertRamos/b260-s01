<?php


//[Section] Comment

// - Single Line Comment 

/*
	This is for
		Multi-line comments
*/


// [Section] Variables
	// Variables are used to contain data.
	// Variables are named location for the stored values.


$name = 'John Smith';
$email = 'johnsmith@gmail.com';


//[Section] Contants

//Constants are used to hold data that are meant to be read-only.
//Constants are defined using the define() function.

define('PI', 3.1416);
define('STUDENT', "Rupert");

$STUDENT = "John";


// [Section] Data Types

//Strings

$state = 'New York';
$country = 'United States of America';
$address = $state. ', ' .$country; // Concatenation via dot symbol.
$address = "$state, $country"; //Concatenation via double quotes.


//Integers

$age = 31;
$headcount = 14;


// Floats

$grade = 98.2;
$distanceInKilometer = 1342.12;


//Boolean
$hasTravelledAbroad = false;
$hasSymptoms = true;

//Null
$spouse = null;
$middleName = null;


//Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);


//Objects
$gradesObject = (object) [
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object) [
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object) [
		'state' => 'New York',
		'country' => 'USA'
	]
];


// [Section] Operators

//Assignment Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// [Section] Functions
// Functions are used to make reusable codes.

function getFullName($firstName, $middleName, $lastName) {
	return "$lastName, $firstName $middleName";
}


// [Section] Selection Control Structures

// Selection control sturctures are used to make code execution dynamic according to predefined conditions.

// If-Elseif-Else Statement

function determineTyphoonIntensity($windSpeed) {
	if ($windSpeed < 30) {
		return 'Not a typhoon yet';
	} else if ($windSpeed <= 61) {
		return 'Tropical depression detected';
	} else if($windSpeed >= 62 && $windSpeed <= 88) {
		return 'Tropical Storm detected';
	} else if($windSpeed >= 89 && $windSpeed <= 117) {
		return 'Severe Tropical Storm detected';
	} else {
		return 'Typhoon detected';
	}
} 


// Conditional (Ternary) Operator
function isUnderAge($age) {
	return ($age < 18) ? true : false;
}

// Switch Statement

function determineComputerUser($compNum) {
	switch ($compNum) {
		case 1:
			return 'Rupert';
			break;
		case 2:
			return 'Edwin';
			break;
		case 3:
			return 'Ellaine';
			break;
		case 4:
			return 'Rafael';
			break;
		case 5:
			return 'Ishan';
			break;
		default:
			return $compNum.' is out of bounds.';
			break;
	}
}

// Try-Catch-Finally

function greeting($string) {
	try {
		if(gettype($string) == "string") {
			echo $string;
		} else {
			throw new Exception("Ooooppsss! Sorry, try again.");
		}
	}
	catch(Exception $e) {
		echo $e->getMessage();
	}
	finally {
		echo "Nice try.";
	}
}